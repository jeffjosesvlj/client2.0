package Dialogs;

import gui.Call;
import gui.Workspace;

import java.awt.EventQueue;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SpringLayout;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JButton;

import data.Project;
import data.ProjectCommunicationInstance;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;

public class CreateProject {
	public String pathDef = null;
	public String path;
	public JFrame frame;
	private JFileChooser chooser;
	private JTextField textField;
	private JButton btnNewButton;
	private JLabel lblNewLabel_2;
	private JTextField textField_1;
	private JButton btnNewButton_1;
	private JLabel lblEnterProjectLocation;
	private JTextField textField_2;
	
	//private Workspace workspace;
	//public static Workspace workspacestatic;
	/**
	 * Launch the application.
	 */
	public void directoryChooser()
	{
		path=new DirectoryChooser().selectedfile;
		textField.setText(path);
	}
	public static void main(String[] args) {
		
		/*EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreateProject window = new CreateProject();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});*/
		CreateProject window = new CreateProject();
		window.frame.setVisible(true);
		System.out.println("Exiting main");
		
	}

	/**
	 * Create the application.
	 */
	public CreateProject() {
		initialize();
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 442, 280);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		
		JLabel lblNewLabel = new JLabel("Create New Project");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel, 26, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel, 27, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(lblNewLabel);
		
		/*JLabel lblNewLabel_1 = new JLabel("Enter Project Location");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_1, 28, SpringLayout.SOUTH, lblNewLabel);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_1, 0, SpringLayout.WEST, lblNewLabel);
		frame.getContentPane().add(lblNewLabel_1);
		
		textField = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, textField, 0, SpringLayout.NORTH, lblNewLabel_1);
		springLayout.putConstraint(SpringLayout.WEST, textField, 34, SpringLayout.EAST, lblNewLabel_1);
		springLayout.putConstraint(SpringLayout.EAST, textField, 279, SpringLayout.EAST, lblNewLabel_1);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		btnNewButton = new JButton("Browse..");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				directoryChooser();
			}
		});
		springLayout.putConstraint(SpringLayout.WEST, btnNewButton, 16, SpringLayout.EAST, textField);
		springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton, 0, SpringLayout.SOUTH, textField);
		frame.getContentPane().add(btnNewButton);
		*/
		lblNewLabel_2 = new JLabel("Enter Project Name");
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_2, 50, SpringLayout.SOUTH, lblNewLabel);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_2, 0, SpringLayout.WEST, lblNewLabel);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		frame.getContentPane().add(lblNewLabel_2);
		
		textField_1 = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, textField_1, 0, SpringLayout.NORTH, lblNewLabel_2);
		springLayout.putConstraint(SpringLayout.WEST, textField_1, 46, SpringLayout.EAST, lblNewLabel_2);
		springLayout.putConstraint(SpringLayout.EAST, textField_1, 196, SpringLayout.EAST, lblNewLabel_2);
		
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		btnNewButton_1 = new JButton("Create");
		springLayout.putConstraint(SpringLayout.WEST, btnNewButton_1, 0, SpringLayout.WEST, lblNewLabel);
		springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton_1, -28, SpringLayout.SOUTH, frame.getContentPane());
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("1");	
				if(textField_1.getText().equals(""))
				{
					System.out.println("Text null");
					JOptionPane.showMessageDialog(null, "Enter the project name", "", JOptionPane.INFORMATION_MESSAGE);
				}
				else if(textField_2.getText().equals(""))
				{
					System.out.println("Path null");
					JOptionPane.showMessageDialog(null, "Enter the project location", "", JOptionPane.INFORMATION_MESSAGE);
				}
				else
				{
					System.out.println("2");	

					try {
						path=textField_2.getText();
						String filePath=new File(path,textField_1.getText()).getAbsolutePath();
						File newProj=new File(path,textField_1.getText());
						System.out.println("File path: "+filePath);
						if(!newProj.exists())
						{
							newProj.mkdir();
						}
						String first="first";
						Project p = new Project(textField_1.getText(), path);
						ProjectCommunicationInstance.launchInstance(p);
						File file = new File(filePath, ".nomedia");
						  if (file.createNewFile()){
						        System.out.println("File is created!");
						      }else{
						        System.out.println("File already exists.");
						      }
						      
						//ProjectDashboard.renderGraphics(new String[]{path,first},p);
						Call.workspace.path=path;
						//System.out.println("Path is "+path);
						//System.out.println("Name is "+textField_1.getText());
						Call.workspace.name=textField_1.getText();
						//Desktop.path=path;
						System.out.println("3");	

						frame.dispose();
						//Remove later: workspacestatic.enable();
						Call.workspace.enable();
						Call.workspace.setupProject();
						System.out.println("4");	

						
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					System.out.println("5");
					
				System.out.println("Ended CreateProject");	
				
				
				}
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		frame.getContentPane().add(btnNewButton_1);
		
		lblEnterProjectLocation = new JLabel("Enter Project Location");
		springLayout.putConstraint(SpringLayout.NORTH, lblEnterProjectLocation, 25, SpringLayout.SOUTH, textField_1);
		springLayout.putConstraint(SpringLayout.WEST, lblEnterProjectLocation, 0, SpringLayout.WEST, lblNewLabel);
		lblEnterProjectLocation.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frame.getContentPane().add(lblEnterProjectLocation);
		
		textField_2 = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, textField_2, 0, SpringLayout.NORTH, lblEnterProjectLocation);
		springLayout.putConstraint(SpringLayout.WEST, textField_2, 0, SpringLayout.WEST, textField_1);
		springLayout.putConstraint(SpringLayout.EAST, textField_2, 0, SpringLayout.EAST, textField_1);
		textField_2.setColumns(10);
		String Os = System.getProperty("os.name");
		if (Os.startsWith("Windows")) {
			pathDef = System.getProperty("user.home") + File.separatorChar + "My Documents";
		}
		else if (Os.startsWith("Linux")) {
			pathDef = System.getProperty("user.home") + File.separatorChar + "Documents";
		}
		
		else if (Os.startsWith("Mac")) {
			pathDef = System.getProperty("user.home") + File.separatorChar + "Documents";
		}
		textField_2.setText(pathDef);
		frame.getContentPane().add(textField_2);
		
		JButton btnNewButton_2 = new JButton("..");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				path=new DirectoryChooser(pathDef,"none").selectedfile;
				textField_2.setText(path);
			}
		});
		springLayout.putConstraint(SpringLayout.NORTH, btnNewButton_2, -1, SpringLayout.NORTH, lblEnterProjectLocation);
		springLayout.putConstraint(SpringLayout.WEST, btnNewButton_2, 6, SpringLayout.EAST, textField_2);
		frame.getContentPane().add(btnNewButton_2);
		
	}
}
