package poi;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.hslf.model.Fill;
import org.apache.poi.hslf.model.Slide;
import org.apache.poi.sl.usermodel.PictureData;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFPictureData;
import org.apache.poi.xslf.usermodel.XSLFPictureShape;
import org.apache.poi.xslf.usermodel.XSLFSlide;

import gui.Call;



public class ImagesToPpt {
	XMLSlideShow ppt;

	public ImagesToPpt(String ext) {
		try
		{
		ppt = new XMLSlideShow();
		ppt.setPageSize(new java.awt.Dimension(720, 540));
		int i=1;
		String f=Call.workspace.name+"_"+i+ext;
		while (new File(Call.workspace.imagesPath,f).exists()) {
			
			String tmp = new File(Call.workspace.imagesPath,f).getAbsolutePath();
			createSlideWithImage(tmp);
			i++;
			f=Call.workspace.name+"_"+i+ext;
		}

		savePpt();
		
		System.out.println("done");
		}
		catch(Exception e) {
			e.printStackTrace();
		}	
		
}
	
/*	Possible future use
 * private static BufferedImage resizeImage(BufferedImage originalImage, int type, int IMG_WIDTH, int IMG_HEIGHT) {
	    BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
	    Graphics2D g = resizedImage.createGraphics();
	    g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
	    g.dispose();

	    return resizedImage;
	}*/
	public void createSlideWithImage(String path) {
		try
		{

		XSLFSlide slide = ppt.createSlide();
		 /*BufferedImage originalImage = ImageIO.read(new File(path));
		 int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
		 BufferedImage resizeImageJpg = resizeImage(originalImage, type, 1024, 768);
	     ImageIO.write(resizeImageJpg, "png", new File(path));*/
		
		File image=new File(path);
		
		byte[] picture = IOUtils.toByteArray(new FileInputStream(image));
		XSLFPictureData pd = ppt.addPicture(picture, PictureData.PictureType.PNG);
		XSLFPictureShape pic = slide.createPicture(pd);
		pic.setAnchor(new Rectangle(720,540));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void savePpt() {
		try {
			FileOutputStream out = new FileOutputStream(Call.workspace.pptPath);
			System.out.println(out.toString());
			ppt.write(out);
			out.close();
			System.out.println("done...");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String args[]) {
		new ImagesToPpt(".png");
	}
}
