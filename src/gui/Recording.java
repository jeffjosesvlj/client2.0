package gui;




import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Timer;

import SoundCapture.SoundCapture;
import Xuggler.DecodeAndSaveAudioVideo;
import Xuggler.VideoCapture;

/**
 *
 * @author Joey
 */
public class Recording {
    SoundCapture sound,currentSound;
    VideoCapture muteVideo,currentMuteVideo;
    long startTime ;
    String audioPath,videoPath,outputPath,currentOpFileName,currentAuFileName,currentViFileName;
    public static boolean record=false;
    public Recording(){
        
		
    }
    public static void copyFile( File from, File to ){
	    try {
			Files.copy( from.toPath(), to.toPath() );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
    public void startContinuousRecording(){
        try {
			audioPath=new File(Call.workspace.audioPath,"recording.wav").getAbsolutePath();
			sound=new SoundCapture(audioPath);
			videoPath = new File(Call.workspace.tempPath,"recording.flv").getAbsolutePath();
			outputPath=new File(Call.workspace.outputPath,"continuous.flv").getAbsolutePath();
			muteVideo=new VideoCapture("recording.flv");
			sound.startRecording();
			muteVideo.start();
            startTime = System.currentTimeMillis();
          
        } catch (Exception ex) {
            Logger.getLogger(Recording.class.getName()).log(Level.SEVERE, null, ex);
        } 
       
    }
    public void startScreenRecording(){
        try {
        	Call.workspace.setVisible(false);
        	String temp="screenrec.wav";
			audioPath=new File(Call.workspace.audioPath,temp).getAbsolutePath();
			sound=new SoundCapture(audioPath);
			temp="screenrec.flv";
			videoPath = new File(Call.workspace.tempPath,temp).getAbsolutePath();
			outputPath=new File(Call.workspace.videosPath,temp).getAbsolutePath();
			muteVideo=new VideoCapture(temp);
			sound.startRecording();
			muteVideo.start();
            startTime = System.currentTimeMillis();
          
        } catch (Exception ex) {
            Logger.getLogger(Recording.class.getName()).log(Level.SEVERE, null, ex);
        } 
       
    }
    public void stopScreenRecording(){
        sound.stopRecording();
        muteVideo.stop();
        
        File f2=new File(videoPath);
        File f1=new File(f2.getName());
        copyFile(f1,f2);
		f1.delete();
        long endTime=System.currentTimeMillis();
        long elapsedMilliSeconds = endTime - startTime;
        double elapsedSeconds = elapsedMilliSeconds / 1000.0;
        try{
        Thread.sleep(1000);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        DecodeAndSaveAudioVideo.stitch(videoPath, audioPath, outputPath);
    }
    public void stitch()
    {
    	Timer t = new Timer(1000 * 4, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // do your reoccuring task
            	
            	try {
            		convertMp4();
            		concatenateVideos();
            		Call.workspace.showOutput();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        });
        t.start(); 
        t.setRepeats(false);
        
    }
    public void startSlide()
    {
    	//Set current recording paths
  
		String num=Call.workspace.currentSlide.getName();
		currentOpFileName=new File(Call.workspace.videosPath,(num+".flv")).getAbsolutePath();
		currentAuFileName=new File(Call.workspace.audioPath,(num+".wav")).getAbsolutePath();
		currentViFileName=new File(Call.workspace.tempPath,(num+".flv")).getAbsolutePath();

    	//Start recording on current paths
    	try {
			currentSound=new SoundCapture(currentAuFileName);
			String fname=new File(currentViFileName).getName();
			currentMuteVideo=new VideoCapture(fname);
			currentSound.startRecording();
			currentMuteVideo.start();
        } catch (Exception ex) {
            Logger.getLogger(Recording.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    public void stopSlide()
    {
    	currentSound.stopRecording();
        currentMuteVideo.stop();
        String fname=new File(currentViFileName).getName();
        File f1=new File(fname);
        File f2=new File(currentViFileName);
        copyFile(f1,f2);
		f1.delete();
        long endTime=System.currentTimeMillis();
        long elapsedMilliSeconds = endTime - startTime;
        double elapsedSeconds = elapsedMilliSeconds / 1000.0;
        try{
        Thread.sleep(1000);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        DecodeAndSaveAudioVideo.stitch(currentViFileName, currentAuFileName, currentOpFileName);
    }
    
    public void convertMp4()
    {
    	System.out.println("Started conversion");
    	File dir = new File(Call.workspace.videosPath);
    	File[] files = dir.listFiles(new FilenameFilter() {
    	    public boolean accept(File dir, String name) {
    	        return name.toLowerCase().endsWith(".flv");
    	    }
    	});
    	
    	for(int i=0;i<files.length;i++)
    	{
    		String f=files[i].getAbsolutePath().replace(".flv", ".mp4");
    		if(!(new File(f).exists()))
    		{
    			DecodeAndSaveAudioVideo.convertFormat(files[i].getAbsolutePath(), f);
    			System.out.println("Converting at "+f);
    		}
    	}
    }
    public void concatenateVideos()
    {
    	System.out.println("Started concatenation");
    	/*File dir = new File(Call.workspace.outputPath);
    	File[] files = dir.listFiles(new FilenameFilter() {
    	    public boolean accept(File dir, String name) {
    	        return name.toLowerCase().endsWith(".mp4");
    	    }
    	});*/
    	/*Wrong logic
    	 * for(int i=0;i<files.length;i++)
    	{
    		String f1,f2;
    		File temp1,temp;
    		temp=new File(Call.workspace.outputPath,"temp.mp4");
    		temp1=new File(Call.workspace.outputPath,"temp1.mp4");
    		
    	}*/
    	/*To be replicated
    	 * int cnt=0;
    	File f1 = null;
    	File temp1,temp;
		temp=new File(Call.workspace.outputPath,"temp.mp4");
		
    	for(Slide x: Call.workspace.librarySlides)
    	{
    		String fname=x.getName()+".mp4";
    		File f=new File(Call.workspace.outputPath,fname);
    		fname=f.getAbsolutePath();
    		if(f.exists())
    		{
    			if(cnt==0)
    			{
    				f1=new File(fname);
    				cnt++;
    			}
    			else if(cnt==1)
    			{
    				Xuggler.ConcatenateAudioAndVideo.concatenate(f1.getAbsolutePath(),f.getAbsolutePath() , temp.getAbsolutePath());
    				cnt++;
    			}
    			else
    			{
    				temp1=new File(Call.workspace.outputPath,"temp1.mp4");
    				Xuggler.ConcatenateAudioAndVideo.concatenate(temp.getAbsolutePath(),f.getAbsolutePath(),temp1.getAbsolutePath());
    				temp.delete();
    				
    				temp=new File(Call.workspace.outputPath,"temp.mp4");
    				temp1.renameTo(temp);
    				
    			}
    		}
    	}*/
    	int cnt=0;
    	File f1 = null;
    	File temp1,temp;
		temp=new File(Call.workspace.tempPath,"temp.mp4");
		
    	for(String x: Call.workspace.Timeline)
    	{
    		String fname=(x.replace("Slide=", "").replace("Video=", ""))+".mp4";
    		File f=new File(Call.workspace.videosPath,fname);
    		fname=f.getAbsolutePath();
    		if(f.exists())
    		{
    			if(cnt==0)
    			{
    				f1=new File(fname);
    				cnt++;
    			}
    			else if(cnt==1)
    			{
    				Xuggler.ConcatenateAudioAndVideo.concatenate(f1.getAbsolutePath(),f.getAbsolutePath() , temp.getAbsolutePath());
    				cnt++;
    			}
    			else
    			{
    				temp1=new File(Call.workspace.tempPath,"temp1.mp4");
    				Xuggler.ConcatenateAudioAndVideo.concatenate(temp.getAbsolutePath(),f.getAbsolutePath(),temp1.getAbsolutePath());
    				temp.delete();
    				
    				temp=new File(Call.workspace.tempPath,"temp.mp4");
    				temp1.renameTo(temp);
    				
    			}
    		}
    	}
    	String f=Call.workspace.name+".mp4";
    	String fileoutput=new File(Call.workspace.outputPath,f).getAbsolutePath();
    	File stitched=new File(fileoutput);
    	if(cnt==1)
    	{
    		copyFile(f1, stitched);
    	}
    	else
    	{
    		copyFile(temp, stitched);
    	}
    	
    	
    }
    public void startRecording()
    {
    	/*if(Call.workspace.continuous)
        	startContinuousRecording();*/
    	startSlide();
    }
    public void stopRecording()
    {
    	/*if(Call.workspace.continuous)
        	stopContinuousRecording();*/
    	stopSlide();
    	/*Timer t = new Timer(1000 * 4, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // do your reoccuring task
            	
            	try {
            		convertMp4();
            		concatenateVideos();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        });
        t.start(); 
        t.setRepeats(false);*/
        Call.workspace.removeTimeline();
    	Call.workspace.populateTimeline();
    }
    public void stopContinuousRecording(){
        sound.stopRecording();
        muteVideo.stop();
        File f1=new File("recording.flv");
        File f2=new File(videoPath);
        copyFile(f1,f2);
		f1.delete();
        long endTime=System.currentTimeMillis();
        long elapsedMilliSeconds = endTime - startTime;
        double elapsedSeconds = elapsedMilliSeconds / 1000.0;
        try{
        Thread.sleep(1000);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        DecodeAndSaveAudioVideo.stitch(videoPath, audioPath, outputPath);
    }
    public void pauseRecording()
    {
    	
    }
    public void unpauseRecording()
    {
    	
    }
}