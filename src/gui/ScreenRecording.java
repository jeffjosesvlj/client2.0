package gui;

import java.io.File;

public class ScreenRecording {
	public String name,path;
	public int order;
	public ScreenRecording(int x)
	{
		order=x;
		name="v"+Integer.toString(order+1);
		String fname=name+".mp4";
		path=new File(Call.workspace.outputPath,fname).getAbsolutePath();		
	}
}
