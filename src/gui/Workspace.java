package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
//import com.sun.star.awt.MouseEvent;
import java.awt.event.KeyEvent;
//import com.sun.star.awt.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;		
import java.io.FileNotFoundException;		
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.net.URL;

import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SpringLayout;
import javax.swing.Timer;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.SoftBevelBorder;

import libreoffice.LibreDesktop;
import org.apache.poi.xslf.usermodel.XMLSlideShow;		
import org.apache.poi.xslf.usermodel.XSLFSlide;
import poi.PPTToImages;
import Dialogs.CreateProject;
import Dialogs.JEnhancedOptionPane;
import Dialogs.OpenAndroid;
import Dialogs.OpenAndroidExp;
import Dialogs.OpenPresentation;
import Dialogs.OpenProject;
import Dialogs.OpenVideo;
import Dialogs.SelectArea;
import Dialogs.JEnhancedOptionPane;

import com.sun.star.io.IOException;
//import com.sun.star.awt.MouseEvent;

public class Workspace extends JFrame {

	private JPanel contentPane, presentationInnerPanel, outputInnerPanel, timelineInnerPanel, videosInnerPanel;
	private JInternalFrame notesFrame, explorerFrame, timelineFrame, slideFrame;
	private JComponent presentationPanel, outputPanel, videosPanel;
	private JMenuBar menuBar;
	private JMenu mnFile, mnEdit, mnOptions, mnExport, mnImport;
	private JMenuItem mntmNewMenuItem, mntmOpenMenuItem, mntmEditPres, mntmVideoFormat, mntmAudioFormat,
			mntmFormat, mntmAndroidexp, mntmPresentation, mntmVideo, mntmAndroid;
	private JCheckBoxMenuItem chckbxmntmActivateContinuousNarration, mntmDecideRecordingArea;
	private JPanel panel;
	private JTabbedPane tabbedPane;
	private JButton btnRecord, videoButton;
	public static String path, location, name;
	public ArrayList<String> individualRecordings, Timeline;
	public ArrayList<CustomPanel> customPanelList; 		
	private XMLSlideShow currentPpt;		
	private java.util.List<XSLFSlide> currentPptSlide;
	private static Recording recording;
	public ArrayList<String> ScreenRecordings;

	public static int x = 0, y = 0, width = 0, height = 0;

	public boolean continuous = true;

	private static JLabel lblSlideDisplay;
	public RecordingFrame recFrame;
	public static ArrayList<Slide> librarySlides;
	public static Slide currentSlide;
	private JButton stopbtn, pauseButton;
	private JButton btnNext;
	private JScrollPane scrollPane;
	public String pptPath = "", audioPath = "", imagesPath = "", outputPath = "", videosPath = "", tempPath;
	private JScrollPane timelineScroll;
	private JButton btnSaveOrder;
	private JButton btnStitch;
	private JButton button;

	/**
	 * Launch the application.
	 */
	public static void copyFile(File from, File to) throws IOException, java.io.IOException {
		// Files.delete(to.toPath());

		Files.copy(from.toPath(), to.toPath());
	}

	public static void changeSlideRight() {
		for (Slide libraryslide : librarySlides) {
			if (libraryslide.getName().equals(currentSlide.getName())) {
				int index = librarySlides.indexOf(libraryslide);
				if (index + 1 < librarySlides.size()) {
					System.out.println(librarySlides.get(index + 1).getName());
					setPreview(librarySlides.get(index + 1).getName());
					recording.stopSlide();
					recording.startSlide();
				} else {
					recording.stopSlide();
				}
				break;
			}

		}

	}

	public static void changeSlideLeft() {
		for (Slide libraryslide : librarySlides) {
			if (libraryslide.getName().equals(currentSlide.getName())) {
				int index = librarySlides.indexOf(libraryslide);
				if ((index - 1) >= 0) {
					setPreview(librarySlides.get(index - 1).getName());
					recording.stopSlide();
					recording.startSlide();
				} else {
					recording.stopSlide();
				}
				break;
			}
		}

	}

	public void showOutput() {
		tabbedPane.setSelectedIndex(2);
		String f = name + ".mp4";
		JLabel l = new JLabel(f);
		l.setIcon(librarySlides.get(0).getThumbnail());
		l.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (Desktop.isDesktopSupported()) {
					Timer t = new Timer(1000 * 4, new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							// do your reoccuring task

							try {
								String f = Call.workspace.name + ".mp4";
								String fileoutput = new File(Call.workspace.outputPath, f).getAbsolutePath();
								File stitched = new File(fileoutput);
								Desktop desktop = Desktop.getDesktop();
								desktop.open(stitched);
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					});
					t.start();
					t.setRepeats(false);
				}

			}
		});
		outputInnerPanel.add(l);
	}

	public void startRecord() {

		recording.startRecording();
		System.out.println("corord: " + x + " " + y + " " + width + " " + height);
		recFrame = new RecordingFrame(x, y, width, height);

		// recFrame=new RecordingFrame(x,y,200,200);
		// System.out.println("Created rec frame");
		// recFrame.showFrame();

		// recFrame.showFrame();
		System.out.println("Leaivng startRecord");

		// recFrame.showFrame();

	}

	public void setPaths() {
		location = path;
		path = new File(location, name).getAbsolutePath();
		String ppt = name + ".pptx";
		pptPath = new File((new File(path, "presentation").getAbsolutePath()), ppt).getAbsolutePath();
		audioPath = new File(path, "audio").getAbsolutePath();
		imagesPath = new File(path, "images").getAbsolutePath();
		outputPath = new File(path, "output").getAbsolutePath();
		videosPath = new File(path, "video").getAbsolutePath();
		tempPath = new File(path, "temp").getAbsolutePath();
	}

	public void openBlankPresentation() {
		try {

			// URL
			// url=Workspace.class.getResource("/resources/BlankPresentation.pptx");
			// System.out.println(url);
			// copyFile(new File(url.getPath()),new File(pptPath));
			File ppt = new File(new File("resources").getAbsolutePath(), "BlankPresentation.pptx");
			copyFile(ppt, new File(pptPath));
		} catch (IOException | java.io.IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void populateExplorerVideos() {
		// Show in explorer
		File dir = new File(Call.workspace.videosPath);
		File[] files = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".mp4");
			}
		});
		String searchString = Call.workspace.name + "_";
		// Add to individualrecordings
		for (int i = 0; i < files.length; i++) {
			if (!files[i].getAbsolutePath().contains(searchString)) {
				String v = files[i].getName().replace(".mp4", "");
				individualRecordings.add(v);
				JLabel l = new JLabel(v);
				l.setName(v);
				l.setIcon(new ImageIcon(Workspace.class.getResource("/resources/video_label.png")));
				l.addMouseListener(new MouseAdapter() {
					public void mouseClicked(MouseEvent e) {
						if (Desktop.isDesktopSupported()) {

							String f = e.getComponent().getName();
							String fileoutput = new File(Call.workspace.videosPath, f).getAbsolutePath();
							File stitched = new File(fileoutput);
							Desktop desktop = Desktop.getDesktop();
							try {
								desktop.open(stitched);
							} catch (java.io.IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}

					}
				});
				videosInnerPanel.add(l);
			}

		}

	}

	public void populateExplorerOutput() {
		// Show in explorer
		File dir = new File(Call.workspace.outputPath);
		File[] files = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".mp4");
			}
		});
		// Add to individualrecordings
		for (int i = 0; i < files.length; i++) {
			String v = files[i].getName().replace(".mp4", "");
			JLabel l = new JLabel(v);
			l.setName(v);
			l.setIcon(librarySlides.get(0).getThumbnail());
			l.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					if (Desktop.isDesktopSupported()) {

						String f = e.getComponent().getName();
						String fileoutput = new File(Call.workspace.outputPath, f).getAbsolutePath();
						File stitched = new File(fileoutput);
						Desktop desktop = Desktop.getDesktop();
						try {
							desktop.open(stitched);
						} catch (java.io.IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}

				}
			});
			videosInnerPanel.add(l);
		}
		videosPanel.revalidate();
		videosPanel.repaint();
	}

	public void populateExplorerSlides() {
		// Remove later

		try {
			PPTToImages ppt = new PPTToImages(pptPath, imagesPath);
			Iterator<String> slides = ppt.files.iterator();

			Slide slide;

			while (slides.hasNext()) {
				String image = slides.next();

				slide = new Slide();
				slide.setFile(image);
				String fileName = new File(image).getName();
				slide.setName(fileName.replace(".jpg", "").replace(".png", ""));
				System.out.println(slide.getName());

				slide.setIcon(new ImageIcon(image));
				addExplorerSlide(slide);

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void populateTimelineSlides() {
		for (Slide libraryslide : this.librarySlides) {
			JPanel p = (JPanel) makeTimelineSlide(libraryslide);
			p.setVisible(true);
			timelineInnerPanel.add(p);

		}
		timelineFrame.getContentPane().revalidate();
		timelineFrame.getContentPane().repaint();
	}

	public void populateTimelineVideos() {
		for (String vidName : individualRecordings) {
			JPanel p = (JPanel) makeTimelineVideo(vidName);
			p.setVisible(true);
			timelineInnerPanel.add(p);

			timelineFrame.getContentPane().revalidate();
			timelineFrame.getContentPane().repaint();
		}
	}

	public void addTimelineVideo(String vidName) {
		JPanel p = (JPanel) makeTimelineVideo(vidName);
		p.setVisible(true);
		timelineInnerPanel.add(p);

		timelineFrame.getContentPane().revalidate();
		timelineFrame.getContentPane().repaint();
	}

	public void addExplorerVideo(String v) {
		individualRecordings.add(v);
		JLabel l = new JLabel(v);
		l.setName(v);
		l.setIcon(new ImageIcon(Workspace.class.getResource("/resources/video_label.png")));
		l.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (Desktop.isDesktopSupported()) {

					String f = e.getComponent().getName();
					String fileoutput = new File(Call.workspace.videosPath, f).getAbsolutePath();
					File stitched = new File(fileoutput);
					Desktop desktop = Desktop.getDesktop();
					try {
						desktop.open(stitched);
					} catch (java.io.IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}

			}
		});
		videosInnerPanel.add(l);
		videosPanel.revalidate();
		videosPanel.repaint();
	}

	public void setupProject() {
		boolean order = false;
		setPaths();
		if (!order) {
			openBlankPresentation();
		}
		populateProject();
	}

	public void repopulateProject()
	{
		clean();
		populateProject();
	}
	public void populateProject() {
		System.out.println("Calling populate");
		populateExplorer();
		populateTimeline();

	}

	public void populateExplorer() {
		populateExplorerSlides();
		populateExplorerVideos();
		populateExplorerOutput();
	}

	public void populateTimeline() {
		Boolean order = false;
		if (!order) {
			populateTimelineSlides();
			populateTimelineVideos();
		} else {
			// Read from csv, populate var, and timeline
		}
	}

	private int roundUp(int n) {
		if ((n % 2) != 0) {
			return (n + 1);
		}
		return n;
	}

	public void setCoordinates(int rectx, int recty, int rectwidth, int rectheight) {
		x = roundUp(rectx);
		y = roundUp(recty);
		width = rectwidth;
		height = rectheight;
	}

	public void disable() {
		notesFrame.setEnabled(false);
		explorerFrame.setEnabled(false);
		timelineFrame.setEnabled(false);
		slideFrame.setEnabled(false);
		presentationPanel.setEnabled(false);
		mnEdit.setEnabled(false);
		mnImport.setEnabled(false);
		// mnOptions.setEnabled(false);
		mnExport.setEnabled(false);
		mntmEditPres.setEnabled(false);
		mntmVideoFormat.setEnabled(false);
		mntmAudioFormat.setEnabled(false);
		mntmDecideRecordingArea.setEnabled(false);
		btnRecord.setEnabled(false);
		stopbtn.setEnabled(false);
		pauseButton.setEnabled(false);
		btnNext.setEnabled(false);
		videoButton.setEnabled(false);
	}

	public void enable() {
		// System.out.print(b);
		notesFrame.setEnabled(true);
		explorerFrame.setEnabled(true);
		timelineFrame.setEnabled(true);
		slideFrame.setEnabled(true);
		presentationPanel.setEnabled(true);
		mnEdit.setEnabled(true);
		mnImport.setEnabled(true);
		// mnOptions.setEnabled(true);
		mnExport.setEnabled(true);
		mntmEditPres.setEnabled(true);
		mntmVideoFormat.setEnabled(true);
		mntmAudioFormat.setEnabled(true);
		mntmDecideRecordingArea.setEnabled(true);
		btnRecord.setEnabled(true);
		stopbtn.setEnabled(true);
		pauseButton.setEnabled(true);
		btnNext.setEnabled(true);
		videoButton.setEnabled(true);
	}

	private void launchPresentation() {

		try {

			File rootPath = new File(System.getProperty("java.class.path"));
			System.out.println("The root path is " + rootPath);
			String openOfficePath = "F:\\IITB\\Nitishavp-lokavidya-desktop-client-4787a50b17fb\\LibreOffice\\program\\simpress.exe",
					template = "F:\\IITB\\Nitishavp-lokavidya-desktop-client-4787a50b17fb\\template\\SpokenTutorial.pptx",
					ffmpeg = "F:\\IITB\\Nitishavp-lokavidya-desktop-client-4787a50b17fb\\ffmpeg\\bin\\ffmpeg.exe";
			if (System.getProperty("os.name").contains("Linux")) {
				System.out.println("In Linux");
				// ffmpeg = new File(new File(new
				// File(rootPath,"ffmpeg").getAbsolutePath(),"bin").getAbsolutePath(),"ffmpeg.exe").getAbsolutePath();
				openOfficePath = "/usr/lib/libreoffice/program/simpress";
				// template = new File(new
				// File(rootPath,"template").getAbsolutePath(),"SpokenTutorial.pptx").getAbsolutePath();

			}
			// To be removed
			else if (System.getProperty("os.name").contains("Windows")) {
				System.out.println("In Windows");
				// ffmpeg = new File(new File(new
				// File(rootPath,"ffmpeg").getAbsolutePath(),"bin").getAbsolutePath(),"ffmpeg.exe").getAbsolutePath();
				// java.net.URL officeURL =
				// Workspace.class.getResource("/LibreOffice/program/simpress.exe");
				openOfficePath = new File(
						new File(new File(rootPath, "LibreOffice").getAbsolutePath(), "program").getAbsolutePath(),
						"simpress.exe").getAbsolutePath();
				// openOfficePath=officeURL.getPath();
				System.out.println(openOfficePath);
				// template = new File(new
				// File(rootPath,"template").getAbsolutePath(),"SpokenTutorial.pptx").getAbsolutePath();
			} else {
				System.out.println("Cannot find OS " + System.getProperty("os.name"));
			}
			// libreoffice.OfficeUNOClientApp_Desktop.addPresentation(projectpath,"second",
			// openOfficePath,projectpath);
			LibreDesktop.launchOfficeInstance(pptPath, openOfficePath);
			// OfficeUNOClientApp_Desktop.launchfirstOfficeInstance(pptPath,openOfficePath,"F:\\IITB\\Nitishavp-lokavidya-desktop-client-4787a50b17fb\\Projects\\Ocean
			// Currents.pptx");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Workspace frame = new Workspace();
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/*
	 * public void refreshTimeline() { populateSlides(); populateVideos();
	 * /*if(added!=1) { //Add label to timeline frame JPanel p=(JPanel)
	 * makeTimelineSlide(slide); p.setVisible(true); timelineInnerPanel.add(p);
	 * timelineFrame.getContentPane().revalidate();
	 * timelineFrame.getContentPane().repaint(); }* }
	 */
	/*
	 * public void startTimeline() { for(Slide libraryslide :
	 * this.librarySlides) { JPanel p=(JPanel) makeTimelineSlide(libraryslide);
	 * p.setVisible(true); timelineInnerPanel.add(p);
	 * 
	 * timelineFrame.getContentPane().revalidate();
	 * timelineFrame.getContentPane().repaint(); } }
	 */
	public void addExplorerSlide(Slide slide) {
		this.librarySlides.add(slide);
		int index = librarySlides.indexOf(slide);
		librarySlides.get(index).setOrder(index);
		System.out.println("Set the slide order to " + index);
		// Add label to explorer frame
		JLabel label = new JLabel();

		label.setName(slide.getName());
		label.setIcon(slide.getThumbnail());

		label.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				System.out.println("Found " + e.getComponent().getName());
				setPreview(e.getComponent().getName());

			}

		});
		System.out.println("Added slide " + slide.getName());
		presentationInnerPanel.add(label);
		presentationPanel.revalidate();
		presentationPanel.repaint();
	}

	public void removeExplorerSlides() {
		Component[] a = presentationInnerPanel.getComponents();
		for (Component j : a) {
			presentationInnerPanel.remove(j);
		}
		this.librarySlides.clear();
		File dir = new File(Call.workspace.imagesPath);
		for (File file : dir.listFiles())
			file.delete();
	}

	public void removeExplorerVideos() {
		// Remove explorer videos
		Component[] a = videosInnerPanel.getComponents();
		for (Component j : a) {
			videosInnerPanel.remove(j);
		}
		// remove from individual recordings
		this.individualRecordings.clear();
	}

	public void removeExplorerOutput() {
		Component[] a = outputInnerPanel.getComponents();
		for (Component j : a) {
			outputInnerPanel.remove(j);
		}
	}

	public void clean() {
		removeExplorer();
		removeTimeline();
	}

	public void removeExplorer() {
		removeExplorerSlides();
		removeExplorerVideos();
		removeExplorerOutput();
	}

	public void removeTimeline() {
		Component[] a = timelineInnerPanel.getComponents();
		for (Component j : a) {
			timelineInnerPanel.remove(j);
		}
		Timeline.clear();
	}

	public void addSlideLibrary(Slide slide) {
		int added = 0, index;
		for (Slide libraryslide : this.librarySlides) {
			System.out.println(libraryslide.getName() + " check " + slide.getName());
			if (libraryslide.getName().equals(slide.getName())) {
				System.out.println("Found slide to replace");
				index = this.librarySlides.indexOf(libraryslide);
				slide.setOrder(index);
				librarySlides.set(index, slide);
				Component[] a = presentationInnerPanel.getComponents();
				for (Component j : a) {
					if (j.getName().equals(slide.getName())) {
						JLabel l = (JLabel) j;
						l.setIcon(slide.getThumbnail());
						break;
					}
				}
				added = 1;
				break;
			}
			// index++;
		}
		if (added != 1) {
			this.librarySlides.add(slide);
			index = librarySlides.indexOf(slide);
			librarySlides.get(index).setOrder(index);
			System.out.println("Set the slide order to " + index);

			// Add label to explorer frame
			JLabel label = new JLabel();

			label.setName(slide.getName());
			label.setIcon(slide.getThumbnail());

			label.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					System.out.println("Found " + e.getComponent().getName());
					setPreview(e.getComponent().getName());

				}

			});
			System.out.println("Added slide " + slide.getName());
			presentationInnerPanel.add(label);
			// scrollPane.revalidate();
			// scrollPane.repaint();
			presentationPanel.revalidate();
			presentationPanel.repaint();

		}
	}

	public static void setPreview(String slidename) {
		Iterator<Slide> slideiterator = librarySlides.iterator();
		while (slideiterator.hasNext()) {
			Slide slide = slideiterator.next();
			if (slide.getName().equals(slidename)) {
				lblSlideDisplay.setIcon(slide.getPreview());
				currentSlide = slide;
				System.out.println("Found slide " + slidename);
				break;
			}
		}
	}

	protected static ImageIcon createImageIcon(String path) {
		java.net.URL imgURL = Workspace.class.getResource(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL);
		} else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}

	protected JComponent makePresPanel() {
		presentationInnerPanel = new JPanel(false);
		presentationInnerPanel.setBackground(new Color(245, 245, 220));
		presentationInnerPanel.setLayout(new GridLayout(0, 1, 10, 10));

		JScrollPane scrollPane = new JScrollPane();
		// presentationPanel.add(scrollPane);
		scrollPane.setViewportView(presentationInnerPanel);
		scrollPane.setVisible(true);
		// panel.add(filler);
		return scrollPane;
	}

	protected JComponent makeVideosPanel() {
		videosInnerPanel = new JPanel(false);
		videosInnerPanel.setBackground(new Color(245, 245, 220));
		videosInnerPanel.setLayout(new GridLayout(0, 1, 10, 10));

		JScrollPane scrollPane = new JScrollPane();
		// presentationPanel.add(scrollPane);
		scrollPane.setViewportView(videosInnerPanel);
		scrollPane.setVisible(true);
		// panel.add(filler);
		return scrollPane;
	}

	protected JComponent makeTimelineSlide(Slide slide) {
		JPanel piece = new JPanel(false);
		piece.setBackground(new Color(245, 245, 245));
		piece.setLayout(new GridLayout(0, 1, 0, 0));
		piece.setSize(140, 150);
		String p = "";
		p = slide.getName().split("_")[1];
		String slidename = "Slide " + p;
		JLabel l1 = new JLabel(slidename);
		JLabel l2 = new JLabel();
		JLabel l3 = new JLabel();
		l3.setFont(new Font("Tahoma", Font.PLAIN, 10));
		l2.setIcon(slide.getSmallImage());
		String fname = slide.getName() + ".wav";
		File f = new File(audioPath, fname);
		if (f.exists()) {
			System.out.println("Could find " + f.getAbsolutePath());
			l3.setIcon(new ImageIcon(Workspace.class.getResource("/resources/audiotrack.png")));
			l3.setText("Audiotrack added");
		} else {
			System.out.println("Couldn't find " + f.getAbsolutePath());
			l3.setIcon(new ImageIcon(Workspace.class.getResource("/resources/no_audiotrack.png")));
			l3.setText("No audiotrack added");
			// l3.setFont(font);
		}
		piece.add(l1);
		piece.add(l2);
		piece.add(l3);
		// piece.setName(slidename);
		String s = "Slide=" + slide.getName();
		System.out.println("Added " + s + " to timeline");
		Timeline.add(s);
		return piece;
	}

	protected JComponent makeTimelineVideo(String y) {
		JPanel piece = new JPanel(false);
		piece.setBackground(new Color(245, 245, 245));
		piece.setLayout(new GridLayout(0, 1, 0, 0));
		piece.setSize(140, 150);

		JLabel l1 = new JLabel(y);
		JLabel l2 = new JLabel();
		JLabel l3 = new JLabel();
		l3.setFont(new Font("Tahoma", Font.PLAIN, 10));
		l2.setIcon(new ImageIcon(Workspace.class.getResource("/resources/video_label.png")));
		l3.setIcon(new ImageIcon(Workspace.class.getResource("/resources/audiotrack.png")));
		l3.setText("Audiotrack added");

		piece.add(l1);
		piece.add(l2);
		piece.add(l3);
		// piece.setName(x);
		Timeline.add("Video=" + y);
		return piece;
	}

	public void saveScreenRecording() {
		recording.stopScreenRecording();
		Object[] options = { "Save", "Discard" };
		// String vidName = (String) JOptionPane.showInputDialog(this,
		// null,"Name the screen recording",0,null,options, options[0]); String
		// vidName = (String) JOptionPane.showInputDialog(this, null,"Name the
		// screen recording",0,null,options, options[0]);
		// String vidName =
		// (String)JOptionPane.showOptionDialog(this,"Test","Name the screen
		// recording", JOptionPane.YES_NO_OPTION,
		// JOptionPane.QUESTION_MESSAGE,null,options, options[0]);
		String vidName = (String) JEnhancedOptionPane.showInputDialog("Name the Screen recording: ",
				new Object[] { "Save", "Discard" });
		System.out.println(vidName);

		if (vidName != null) {/*
								 * JPanel p=(JPanel) makeTimelineVideo(vidName);
								 * p.setVisible(true);
								 * timelineInnerPanel.add(p);
								 * individualRecordings.add(vidName);
								 * timelineFrame.getContentPane().revalidate();
								 * timelineFrame.getContentPane().repaint();
								 */
			String fname = "screenrec.flv";
			File f = new File(Call.workspace.videosPath, fname);
			fname = vidName + ".flv";
			File f1 = new File(Call.workspace.videosPath, fname);
			f.renameTo(f1);
			addExplorerVideo(vidName);
			addTimelineVideo(vidName);
		} else {
			String fname = "screenrec.flv";
			File f = new File(Call.workspace.videosPath, fname);
			f.delete();
		}
	}

	protected JComponent makeOutputPanel() {
		outputInnerPanel = new JPanel(false);
		outputInnerPanel.setBackground(new Color(245, 245, 220));
		outputInnerPanel.setLayout(new GridLayout(0, 1, 0, 0));

		JScrollPane scrollPane = new JScrollPane();
		// presentationPanel.add(scrollPane);
		scrollPane.setViewportView(outputInnerPanel);
		scrollPane.setVisible(true);
		// panel.add(filler);
		return scrollPane;
	}

	/**
	 * Create the frame.
	 */
	public Workspace() {
		recording = new Recording();
		// selfRef=this;
		recording = new Recording();
		librarySlides = new ArrayList<>();
		individualRecordings = new ArrayList<String>();
		Timeline = new ArrayList<String>();
		customPanelList= new ArrayList<CustomPanel>();
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1210, 710);

		menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		mnFile = new JMenu("File");
		menuBar.add(mnFile);

		mntmNewMenuItem = new JMenuItem("New Project");
		mnFile.add(mntmNewMenuItem);
		mntmNewMenuItem.setAccelerator(
				javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Remove later: CreateProject.workspacestatic = selfRef;
				CreateProject.main(null);
				System.out.println("Enable called");
				// enable();
			}
		});

		mntmOpenMenuItem = new JMenuItem("Open Project");
		mnFile.add(mntmOpenMenuItem);
		mntmOpenMenuItem.setAccelerator(
				javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
		mntmOpenMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				OpenProject.main(null);

			}
		});

		mntmEditPres = new JMenuItem("Edit Presentation");
		mnFile.add(mntmEditPres);
		mntmEditPres.setAccelerator(
				javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
		mntmEditPres.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {		
					FileOutputStream out = new FileOutputStream(pptPath);		
					currentPpt.write(out);		
					out.close();		
					System.out.println("presentation updated..");		
				} catch (FileNotFoundException e1) {		
					e1.printStackTrace();		
				} catch (java.io.IOException e1) {		
					// TODO Auto-generated catch block		
					e1.printStackTrace();		
				}
				launchPresentation();
			}
		});
		mnEdit = new JMenu("Edit");
		menuBar.add(mnEdit);

		mnOptions = new JMenu("Options");
		menuBar.add(mnOptions);

		mnImport = new JMenu("Import");
		mntmPresentation = new JMenuItem("Presentation ...");
		mntmVideo = new JMenuItem("Video ...");
		mntmAndroid = new JMenuItem("Android Project ...");
		mnImport.add(mntmPresentation);
		mnImport.add(mntmVideo);
		mnImport.add(mntmAndroid);
		menuBar.add(mnImport);

		mntmPresentation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Called");
				try {
					OpenPresentation.main(null);
				} catch (NullPointerException ex) {
					// logger.log(Level.SEVERE, e.getMessage(), e);
					ex.printStackTrace();
				}
			}
		});

		mntmVideo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				OpenVideo.main(null);

			}
		});

		mntmAndroid.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				OpenAndroid.main(null);

			}
		});

		mntmVideoFormat = new JMenuItem("Video Format");
		mnOptions.add(mntmVideoFormat);

		mntmAudioFormat = new JMenuItem("Audio Format");
		mnOptions.add(mntmAudioFormat);

		chckbxmntmActivateContinuousNarration = new JCheckBoxMenuItem("Activate Continuous Narration");
		chckbxmntmActivateContinuousNarration.setSelected(true);
		chckbxmntmActivateContinuousNarration.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Selected: " + e.getActionCommand());
				if (!chckbxmntmActivateContinuousNarration.isSelected()) {
					continuous = false;
				} else {
					continuous = true;
				}
			}
		});
		mnOptions.add(chckbxmntmActivateContinuousNarration);

		mntmDecideRecordingArea = new JCheckBoxMenuItem("Decide Recording Area");
		mnOptions.add(mntmDecideRecordingArea);
		mntmDecideRecordingArea.setSelected(false);
		mntmDecideRecordingArea.addItemListener(new ItemListener() {

			public void itemStateChanged(ItemEvent e) {
				if (mntmDecideRecordingArea.isSelected()) {
					setVisible(false);
					Timer t = new Timer(500 * 1, new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							// do your reoccuring task

							try {
								SelectArea.main(null);
								mntmDecideRecordingArea.setSelected(true);
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					});
					t.start();
					t.setRepeats(false);
				} else {
					System.out.println("Resetting coordinates");
					x = 0;
					y = 0;
					width = 0;
					height = 0;
				}
			}
		});
		mnExport = new JMenu("Export");
		menuBar.add(mnExport);

		mntmFormat = new JMenuItem("Video Format");
		mnExport.add(mntmFormat);

		mntmAndroidexp = new JMenuItem("Android App");
		mnExport.add(mntmAndroidexp);
		mntmAndroidexp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				OpenAndroidExp.main(null);

			}
		});
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		// contentPane.setBounds(0, 0, 1300, 780);
		setContentPane(contentPane);

		explorerFrame = new JInternalFrame("Project Explorer");
		explorerFrame.getContentPane().setBackground(new Color(139, 176, 244));// ,149,237));
		explorerFrame.setBounds(5, 5, 300, 412);
		explorerFrame.setVisible(true);

		slideFrame = new JInternalFrame("Recorder");
		slideFrame.setBounds(315, 5, 587, 412);
		slideFrame.getContentPane().setBackground(Color.WHITE);
		SpringLayout springLayout_1 = new SpringLayout();
		slideFrame.getContentPane().setLayout(springLayout_1);

		panel = new JPanel();
		springLayout_1.putConstraint(SpringLayout.NORTH, panel, -43, SpringLayout.SOUTH, slideFrame.getContentPane());
		springLayout_1.putConstraint(SpringLayout.WEST, panel, 0, SpringLayout.WEST, slideFrame.getContentPane());
		springLayout_1.putConstraint(SpringLayout.SOUTH, panel, 0, SpringLayout.SOUTH, slideFrame.getContentPane());
		springLayout_1.putConstraint(SpringLayout.EAST, panel, 592, SpringLayout.WEST, slideFrame.getContentPane());
		panel.setBackground(new Color(220, 220, 220));
		slideFrame.getContentPane().add(panel);
		SpringLayout sl_panel = new SpringLayout();
		panel.setLayout(sl_panel);

		btnRecord = new JButton("");
		sl_panel.putConstraint(SpringLayout.NORTH, btnRecord, 10, SpringLayout.NORTH, panel);
		sl_panel.putConstraint(SpringLayout.WEST, btnRecord, 10, SpringLayout.WEST, panel);
		sl_panel.putConstraint(SpringLayout.SOUTH, btnRecord, -7, SpringLayout.SOUTH, panel);
		btnRecord.setSelectedIcon(new ImageIcon(Workspace.class.getResource("/resources/record.png")));
		btnRecord.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnRecord.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Gif.main(null);
				// startRecord();

				System.out.println("returnde");
				Timer t = new Timer(1000 * 1, new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						// do your reoccuring task

						startRecord();

						System.out.println("returnde");
					}
				});
				t.start();
				t.setRepeats(false);

				System.out.println("outside thread,,,");

			}
		});
		btnRecord.setIcon(new ImageIcon(Workspace.class.getResource("/resources/record.png")));
		btnRecord.setBackground(new Color(176, 196, 222));
		panel.add(btnRecord);
		lblSlideDisplay = new JLabel("");
		springLayout_1.putConstraint(SpringLayout.NORTH, lblSlideDisplay, 10, SpringLayout.NORTH,
				slideFrame.getContentPane());
		springLayout_1.putConstraint(SpringLayout.WEST, lblSlideDisplay, -565, SpringLayout.EAST,
				slideFrame.getContentPane());
		springLayout_1.putConstraint(SpringLayout.SOUTH, lblSlideDisplay, -6, SpringLayout.NORTH, panel);
		springLayout_1.putConstraint(SpringLayout.EAST, lblSlideDisplay, -12, SpringLayout.EAST,
				slideFrame.getContentPane());

		stopbtn = new JButton("");
		sl_panel.putConstraint(SpringLayout.WEST, stopbtn, 61, SpringLayout.WEST, panel);
		sl_panel.putConstraint(SpringLayout.EAST, btnRecord, -6, SpringLayout.WEST, stopbtn);
		sl_panel.putConstraint(SpringLayout.NORTH, stopbtn, 10, SpringLayout.NORTH, panel);
		sl_panel.putConstraint(SpringLayout.SOUTH, stopbtn, -7, SpringLayout.SOUTH, panel);
		stopbtn.setSelectedIcon(new ImageIcon(Workspace.class.getResource("/resources/stop.png")));
		stopbtn.setIcon(new ImageIcon(Workspace.class.getResource("/resources/stop.png")));
		stopbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				recording.stopRecording();
				recFrame.hideFrame();
			}
		});
		stopbtn.setFont(new Font("Tahoma", Font.PLAIN, 11));

		stopbtn.setBackground(new Color(176, 196, 222));
		panel.add(stopbtn);

		btnNext = new JButton("");
		btnNext.setIcon(new ImageIcon(Workspace.class.getResource("/resources/navigate.png")));
		sl_panel.putConstraint(SpringLayout.EAST, btnNext, -40, SpringLayout.EAST, panel);
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/*
				 * for(Slide libraryslide: librarySlides) {
				 * if(libraryslide.getName().equals(currentSlide.getName())) {
				 * int index=librarySlides.indexOf(libraryslide);
				 * if(index+1<librarySlides.size()) {
				 * System.out.println(librarySlides.get(index+1).getName());
				 * setPreview(librarySlides.get(index+1).getName());
				 * recording.stopSlide(); recording.startSlide(); } else {
				 * recording.stopSlide(); } break; }
				 * 
				 * }
				 */
				changeSlideRight();
			}
		});
		sl_panel.putConstraint(SpringLayout.NORTH, btnNext, 10, SpringLayout.NORTH, panel);
		sl_panel.putConstraint(SpringLayout.WEST, btnNext, -98, SpringLayout.EAST, panel);
		sl_panel.putConstraint(SpringLayout.SOUTH, btnNext, -7, SpringLayout.SOUTH, panel);
		btnNext.setSelectedIcon(new ImageIcon(Workspace.class.getResource("/resources/navigate.png")));
		btnNext.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnNext.setBackground(new Color(176, 196, 222));
		panel.add(btnNext);

		pauseButton = new JButton("");
		pauseButton.setIcon(new ImageIcon(Workspace.class.getResource("/resources/pause.png")));
		pauseButton.setEnabled(false);
		sl_panel.putConstraint(SpringLayout.WEST, pauseButton, 110, SpringLayout.WEST, panel);
		sl_panel.putConstraint(SpringLayout.EAST, pauseButton, -339, SpringLayout.WEST, btnNext);
		sl_panel.putConstraint(SpringLayout.EAST, stopbtn, -6, SpringLayout.WEST, pauseButton);
		sl_panel.putConstraint(SpringLayout.NORTH, pauseButton, 10, SpringLayout.NORTH, panel);
		sl_panel.putConstraint(SpringLayout.SOUTH, pauseButton, -7, SpringLayout.SOUTH, panel);
		pauseButton.setSelectedIcon(new ImageIcon(Workspace.class.getResource("/resources/pause.png")));
		pauseButton.setFont(new Font("Tahoma", Font.PLAIN, 11));
		pauseButton.setBackground(new Color(176, 196, 222));
		panel.add(pauseButton);

		videoButton = new JButton("");
		videoButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// ScreenRecording t=new
				// ScreenRecording(ScreenRecordings.size());
				// ScreenRecordings.add(t);
				// recording.startScreenRecording(t.name);
				recording.startScreenRecording();

			}
		});
		sl_panel.putConstraint(SpringLayout.EAST, videoButton, 205, SpringLayout.WEST, panel);
		videoButton.setIcon(new ImageIcon(Workspace.class.getResource("/resources/videocam.png")));
		videoButton.setEnabled(false);
		sl_panel.putConstraint(SpringLayout.WEST, videoButton, 160, SpringLayout.WEST, panel);
		sl_panel.putConstraint(SpringLayout.EAST, pauseButton, -6, SpringLayout.WEST, videoButton);
		sl_panel.putConstraint(SpringLayout.NORTH, videoButton, 10, SpringLayout.NORTH, panel);
		sl_panel.putConstraint(SpringLayout.SOUTH, videoButton, -7, SpringLayout.SOUTH, panel);
		videoButton.setSelectedIcon(new ImageIcon(Workspace.class.getResource("/resources/pause.png")));
		videoButton.setFont(new Font("Tahoma", Font.PLAIN, 11));
		videoButton.setBackground(new Color(176, 196, 222));
		panel.add(videoButton);

		lblSlideDisplay.setIcon(new ImageIcon(Workspace.class.getResource("/resources/start.jpg")));
		slideFrame.getContentPane().add(lblSlideDisplay);
		slideFrame.setVisible(true);

		tabbedPane = new JTabbedPane();
		tabbedPane.setBounds(10, 22, 264, 349);
		tabbedPane.setBackground(Color.LIGHT_GRAY);
		ImageIcon icon = createImageIcon("images/middle.gif");
		contentPane.setLayout(null);
		explorerFrame.getContentPane().setLayout(null);

		presentationPanel = makePresPanel();
		tabbedPane.addTab("Presentation", icon, presentationPanel, "Edit Presentation in LibreOffice");
		tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);
		// tabbedPane.addTab("Presentation", icon, panel2,"Does nothing");
		explorerFrame.getContentPane().add(tabbedPane);

		videosPanel = makeVideosPanel();
		tabbedPane.addTab("Videos", icon, videosPanel, "View videos");
		tabbedPane.setMnemonicAt(0, KeyEvent.VK_2);
		// tabbedPane.addTab("Presentation", icon, panel2,"Does nothing");
		explorerFrame.getContentPane().add(tabbedPane);

		outputPanel = makeOutputPanel();
		tabbedPane.addTab("Output", icon, outputPanel, "View output video");
		tabbedPane.setMnemonicAt(1, KeyEvent.VK_3);
		// tabbedPane.addTab("Presentation", icon, panel2,"Does nothing");
		explorerFrame.getContentPane().add(tabbedPane);

		contentPane.add(explorerFrame);

		contentPane.add(slideFrame);

		timelineFrame = new JInternalFrame("Video Outline");
		timelineFrame.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		timelineFrame.getContentPane().setBackground(new Color(176, 224, 230));
		timelineFrame.getContentPane().setLayout(null);

		timelineScroll = new JScrollPane();
		timelineScroll.setBounds(12, 12, 1030, 159);
		timelineScroll.setBackground(SystemColor.control);

		timelineInnerPanel = new JPanel();
		// timelineInnerPanel.setBounds(12, 12, 1144, 159);
		timelineInnerPanel.setBackground(SystemColor.control);
		timelineScroll.setViewportView(timelineInnerPanel);
		timelineInnerPanel.setLayout(new GridLayout(1, 0, 10, 10));
		timelineScroll.setVisible(true);

		timelineFrame.getContentPane().add(timelineScroll);

		JPanel StitchToolbarpanel = new JPanel();
		StitchToolbarpanel.setBackground(SystemColor.control);
		StitchToolbarpanel.setBounds(1055, 12, 101, 159);
		timelineFrame.getContentPane().add(StitchToolbarpanel);
		StitchToolbarpanel.setLayout(null);

		btnSaveOrder = new JButton("Save order");
		btnSaveOrder.setBounds(3, 19, 96, 23);
		btnSaveOrder.setFont(new Font("Dialog", Font.BOLD, 10));
		btnSaveOrder.setBackground(new Color(245, 245, 245));
		StitchToolbarpanel.add(btnSaveOrder);

		btnStitch = new JButton("Stitch");
		btnStitch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				recording.stitch();

			}
		});
		btnStitch.setBounds(3, 54, 96, 23);
		btnStitch.setFont(new Font("Dialog", Font.BOLD, 10));
		btnStitch.setBackground(new Color(245, 245, 245));
		StitchToolbarpanel.add(btnStitch);

		button = new JButton("Exchange");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(CustomPanel.highlightCount == 2) {		
					System.out.println("Swap time..");		
					System.out.println("1: "+CustomPanel.selected[0] + " 2: "+CustomPanel.selected[1]);		
					System.out.println("Size: "+customPanelList.size());		
					CustomPanel panel1 = customPanelList.get(CustomPanel.selected[0]);		
					CustomPanel panel2 = customPanelList.get(CustomPanel.selected[1]);		
					panel1.isHighlighted=false;		
					panel2.isHighlighted=false;		
					panel1.setBorder(panel1.blackBorder);		
					panel2.setBorder(panel2.blackBorder);		
							
							
					/*String temp = Timeline.get(CustomPanel.selected[0]);		
					Timeline.set(CustomPanel.selected[0], Timeline.get(CustomPanel.selected[1]));		
					Timeline.set(CustomPanel.selected[1], temp);*/		
						
							
					File image1 = new File(imagesPath+"/"+name+"_"+CustomPanel.selected[0]+".png");		
					File image2 = new File(imagesPath+"/"+name+"_"+CustomPanel.selected[1]+".png");		
					File imageTemp = new File(imagesPath+"/temp.png");		
					try {		
						Workspace.copyFile(image1, imageTemp);		
						Workspace.copyFile(image2, image1);		
						Workspace.copyFile(imageTemp, image2);		
					} catch (IOException e1) {		
						e1.printStackTrace();		
					} catch (java.io.IOException e1) {		
						e1.printStackTrace();		
					}		
					System.out.println("Copy image complete");		
					try{		
					File audio1 = new File(audioPath+"/"+name+"_"+CustomPanel.selected[0]+".wav");		
					File audio2 = new File(audioPath+"/"+name+"_"+CustomPanel.selected[1]+".wav");		
					File audioTemp = new File(audioPath+"/temp.wav");		
					try {		
						Workspace.copyFile(audio1, audioTemp);		
						Workspace.copyFile(audio2, audio1);		
						Workspace.copyFile(audioTemp, audio2);		
					} catch (IOException e1) {		
						e1.printStackTrace();		
					} catch (java.io.IOException e1) {		
						e1.printStackTrace();		
					}		
					}		
					catch(Exception e1) {		
						System.out.println("No audio");		
					}		
							
						
					Slide slide = librarySlides.get(CustomPanel.selected[0]);		
					librarySlides.set(CustomPanel.selected[0], librarySlides.get(CustomPanel.selected[1]));		
					librarySlides.set(CustomPanel.selected[1], slide);		
							
					int tId = panel1.id;		
					panel1.id = panel2.id;		
					panel2.id = tId;		
				    		
					Component compPanel1 [] = panel1.getComponents();		
					Component compPanel2 [] = panel2.getComponents();		
					JLabel panel1Label = (JLabel)compPanel1[0];		
					JLabel panel2Label = (JLabel)compPanel2[0];		
					String tempText = panel2Label.getText();		
					panel2Label.setText(panel1Label.getText());		
					panel1Label.setText(tempText);		
					customPanelList.set(CustomPanel.selected[0], panel2);		
					customPanelList.set(CustomPanel.selected[1],panel1);		
					XSLFSlide tempSlide1 = currentPptSlide.get(CustomPanel.selected[0]);		
					XSLFSlide tempSlide2 = currentPptSlide.get(CustomPanel.selected[1]);		
							
							
							
							
					if (CustomPanel.selected[0] > CustomPanel.selected[1]) {		
					currentPpt.setSlideOrder(tempSlide1, CustomPanel.selected[1]);		
					currentPpt.setSlideOrder(tempSlide2, CustomPanel.selected[0]);		
					}		
					else {		
						currentPpt.setSlideOrder(tempSlide2, CustomPanel.selected[0]);		
						currentPpt.setSlideOrder(tempSlide1, CustomPanel.selected[1]);		
					}		
				//	CustomPanel.slideCount=0;		
					removeTimeline();		
					CustomPanel.highlightCount=0;		
							
					Iterator <CustomPanel> iterator = customPanelList.iterator();		
					while(iterator.hasNext()){		
						JPanel panel = iterator.next();		
						timelineInnerPanel.add(panel);		
					}		
							
				//	populateTimeline();		
					timelineFrame.getContentPane().revalidate();		
			        timelineFrame.getContentPane().repaint();		
			        CustomPanel.selected[0]=-1;		
			        CustomPanel.selected[1] = -1;		
				}		
						
				
			}
		});
		button.setBounds(3, 89, 96, 23);
		button.setFont(new Font("Dialog", Font.BOLD, 10));
		button.setBackground(new Color(245, 245, 245));
		StitchToolbarpanel.add(button);

		JButton btnDelete = new JButton("Delete");
		btnDelete.setBounds(3, 124, 96, 23);
		btnDelete.setBackground(new Color(245, 245, 245));
		btnDelete.setFont(new Font("Dialog", Font.BOLD, 10));
		StitchToolbarpanel.add(btnDelete);
		timelineFrame.setBounds(10, 428, 1174, 211);
		contentPane.add(timelineFrame);

		notesFrame = new JInternalFrame("Speaker's Notes");
		notesFrame.getContentPane().setBackground(new Color(176, 196, 222));
		notesFrame.setBounds(907, 5, 277, 412);
		contentPane.add(notesFrame);
		notesFrame.setVisible(true);
		timelineFrame.setVisible(true);

		this.disable();
	}
}