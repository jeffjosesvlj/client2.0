package data;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import gui.Call;

public class ConverttoAndroid {

	public static void copyFile( File from, File to ) {
		 //  Files.delete(to.toPath());
	    	
	    	try {
				Files.copy( from.toPath(), to.toPath() );
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	public ConverttoAndroid(String location) {
		// TODO Auto-generated constructor stub
		File proj=new File(location,Call.workspace.name);
		proj.mkdir();
		
		File images=new File(proj.getAbsolutePath(),"images");
		images.mkdir();
		File theDir=new File(Call.workspace.imagesPath);
		for(File f:theDir.listFiles())
		{
			if(f.exists())
			{
				String fname=f.getName().replace("_", ".");
				File fdest=new File(images.getAbsolutePath(),fname);
				copyFile(f, fdest);
			}
		}
		
		File audio=new File(proj.getAbsolutePath(),"audio");
		audio.mkdir();
		theDir=new File(Call.workspace.audioPath);
		for(File f:theDir.listFiles())
		{
			if(f.exists())
			{
				String fname=f.getName().replace("_", ".");
				File fdest=new File(audio.getAbsolutePath(),fname);
				copyFile(f, fdest);
			}
		}
		
		File output=new File(proj.getAbsolutePath(),"output");
		output.mkdir();
		theDir=new File(Call.workspace.outputPath);
		for(File f:theDir.listFiles())
		{
			if(f.exists())
			{
				String fname=f.getName().replace("_", ".");
				File fdest=new File(output.getAbsolutePath(),fname);
				copyFile(f, fdest);
			}
		}
	}
}
