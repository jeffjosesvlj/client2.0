package data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import org.apache.commons.io.FileUtils;
import data.FFMPEGWrapper;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;


public class Project {

	String name;
	String projectLocalURL;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProjectLocalURL() {
		return projectLocalURL;
	}

	public void setProjectLocalURL(String projectLocalURL) {
		this.projectLocalURL = projectLocalURL;
	}
	
	/*
	 * Project consists of 
	 *  Documents add IO capabilites
	 *  	* ProjectOrder
	 *  		Store videos.
	 *  	* Narration script parser  Slide Annotation. 
	 */
	public Project(String name,String projectsDir)
	{
		this.name= name;
		this.projectLocalURL= new File(projectsDir,name).getAbsolutePath();
		createDirectories();
		
	}
	
	private void createDirectories() {
		File theDir = new File(this.getProjectLocalURL());
		System.out.println(theDir.getAbsolutePath());
		// if the directory does not exist, create it
		if (!theDir.exists()) {
		    System.out.println("creating directory: " + this.name);
		    boolean result = false;

		    try{
		        theDir.mkdir();
		        result = true;
		    } 
		    catch(SecurityException se){
		        //handle it
		    }        
		    if(result) {    
		        System.out.println("DIR created");  
		    }
		}
		theDir = new File(this.getProjectLocalURL(),"presentation");
		
		
		
		//Create directories presentation , video ,  novice_check , temp , tracked
		//Creating temp directory if does not exists.
				theDir = new File(this.getProjectLocalURL(),"temp");
				System.out.println(theDir.getAbsolutePath());
				// if the directory does not exist, create it
				if (!theDir.exists()) {
				    System.out.println("creating directory: " + "temp");
				    boolean result = false;

				    try{
				        theDir.mkdir();
				        result = true;
				    } 
				    catch(SecurityException se){
				        //handle it
				    }        
				    if(result) {    
				        System.out.println("DIR created");  
				    }
				}
				theDir = new File(this.getProjectLocalURL(),"presentation");

				// if the directory does not exist, create it
				if (!theDir.exists()) {
				    System.out.println("creating directory: " + "presentation");
				    boolean result = false;

				    try{
				        theDir.mkdir();
				        result = true;
				    } 
				    catch(SecurityException se){
				        //handle it
				    }        
				    if(result) {    
				        System.out.println("DIR created");  
				    }
				}
				theDir = new File(this.getProjectLocalURL(),"images");

				// if the directory does not exist, create it
				if (!theDir.exists()) {
				    System.out.println("creating directory: " + "novice_check");
				    boolean result = false;

				    try{
				        theDir.mkdir();
				        result = true;
				    } 
				    catch(SecurityException se){
				        //handle it
				    }        
				    if(result) {    
				        System.out.println("DIR created");  
				    }
				}
				theDir = new File(this.getProjectLocalURL(),"video");

				// if the directory does not exist, create it
				if (!theDir.exists()) {
				    System.out.println("creating directory: " + "video");
				    boolean result = false;

				    try{
				        theDir.mkdir();
				        result = true;
				    } 
				    catch(SecurityException se){
				        //handle it
				    }        
				    if(result) {    
				        System.out.println("DIR created");  
				    }
				}
				theDir = new File(this.getProjectLocalURL(),"audio");
				System.out.println(theDir.getAbsolutePath());
				// if the directory does not exist, create it
				if (!theDir.exists()) {
				    System.out.println("creating directory: " + "audio");
				    boolean result = false;

				    try{
				        theDir.mkdir();
				        result = true;
				    } 
				    catch(SecurityException se){
				        //handle it
				    }        
				    if(result) {    
				        System.out.println("DIR created");  
				    }
				}
				theDir = new File(this.getProjectLocalURL(),"output");
				System.out.println(theDir.getAbsolutePath());
				// if the directory does not exist, create it
				if (!theDir.exists()) {
				    System.out.println("creating directory: " + "output");
				    boolean result = false;

				    try{
				        theDir.mkdir();
				        result = true;
				    } 
				    catch(SecurityException se){
				        //handle it
				    }        
				    if(result) {    
				        System.out.println("DIR created");  
				    }
				}
	}
	
	/*public static void importProject(String importPath, String projectName, String projectDir)
	{
		File rootProjectDir = new File(importPath);
		String[] names = rootProjectDir.list();
		ArrayList<String> checkList = new ArrayList<String>();
		checkList.add("images");
		checkList.add("video");
		checkList.add("presentation");
		checkList.add("resources");
		int counter = checkList.size();
		for(String name : names)
		{
		    if (new File(importPath, name).isDirectory())
		    {
		        System.out.println(name);
		        if(containsInList(checkList, name))
		        {
		        	checkList.remove(name);
		        	counter--;
		        }
		    }
		}
		if (counter==0)
		{
			File srcDir = new File(importPath);
			File destDir = new File(projectDir,projectName);
			try {
				FileUtils.copyDirectory(srcDir, destDir);
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("Success");
		}
		else
			System.out.println("Invalid");
	}
	
	private static boolean containsInList(List<String> list,String s)
	{
		if (list.contains(s)) {
		   return true;
		} else {
		   return false;
		}
	}
	
	
	
	public void stitch()
	{
		String path= (new File(this.getProjectLocalURL()).getParentFile().getParentFile().getAbsolutePath());
		System.out.println(path);
		FFMPEGWrapper ffmpegWrapper = new FFMPEGWrapper(path);
		File resdir= new File(this.getProjectLocalURL(),"resources");
		String[] files=resdir.list();

		/*
		 * In the first pass, join images,audio to videos and append them to the res file.
		 * Second pass, collect the videos
		 * Third. Execute concatenate over all the videos.
		 * 
		 *
		generateVideosForSlides(ffmpegWrapper, resdir);
		List<String> videoList=getVideoList(resdir);
		 try {
             FileOutputStream out = new FileOutputStream(new File( new File(this.getProjectLocalURL(),"temp").getAbsolutePath(), "order.txt"));
             for(int i=0;i<videoList.size();i++) {
                 String data="file '" +videoList.get(i) +"'\n";
                 out.write(data.getBytes());
             }
             out.close();
         } catch (IOException e) {
             e.printStackTrace();
         }
		 String orderFile=new File( new File(this.getProjectLocalURL(),"temp").getAbsolutePath(), "order.txt").getAbsolutePath();
		 System.out.println("Stitching the final");
		 String finalOutputVideo = new File(new File(this.getProjectLocalURL(),"output").getAbsolutePath(), this.getName()+".mp4").getAbsolutePath();
		 try {
			ffmpegWrapper.stitchVideo(videoList, orderFile, finalOutputVideo);
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		 
	}

	private List<String> getVideoList(File resdir) {
		String[] files=resdir.list();
		List<String> videoList= new ArrayList<String>();
		Arrays.sort(files);
		for(String f: files)
		{
			Properties prop = new Properties();
			InputStream input = null;
			//System.out.println(f);
			if(f.matches("^[0-9].res"))
			try {
				input = new FileInputStream(new File(resdir.getAbsolutePath(),f));
				// load a properties file
				prop.load(input);
				// get the property value and print it out
				String videoPath=prop.getProperty("video");
				videoList.add(videoPath);
			} catch (IOException ex) {
				ex.printStackTrace();
			} finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return videoList;
	}

	private void generateVideosForSlides(FFMPEGWrapper ffmpegWrapper, File resdir) {
		String[] files=resdir.list();
		for(String f: files)
		{
			System.out.println("Inside Stitch:"+f);
			Properties prop = new Properties();
			InputStream input = null;
			//System.out.println(f);
			if(f.matches("^[0-9].res"))
			try {
				input = new FileInputStream(new File(resdir.getAbsolutePath(),f));
				// load a properties file
				prop.load(input);
				// get the property value and print it out
				String type=prop.getProperty("type");
				if(type.equals("slide"))
				{
					if(prop.getProperty("video")==null)
					{
						//Stitch the audio and the image and set the video path to the video property.
						String imagePath = prop.getProperty("image");
						System.out.println("Image path is "+imagePath);
						String audioPath = prop.getProperty("audio");
						System.out.println("Audio path is "+audioPath);
						String videoPath=new File(new File(this.getProjectLocalURL(),"presentation").getAbsolutePath(),f.split("\\.")[0]+".mp4").getAbsolutePath();
						System.out.println("Video path is "+videoPath);
						String tempPath=new File(new File(this.getProjectLocalURL(),"temp").getAbsolutePath(),"temp.mp4").getAbsolutePath();
						System.out.println("Temp path is "+tempPath);
						
						ffmpegWrapper.stitchImageAndAudio(imagePath,
									audioPath,
									videoPath,
									tempPath);
						writePropertiesFile("video",new File(new File(this.getProjectLocalURL(),"presentation").getAbsolutePath(),f.split("\\.")[0]+".mp4").getAbsolutePath(),new File(resdir.getAbsolutePath(),f).getAbsolutePath());
					}
					System.out.println("Stitched for "+new File(resdir.getAbsolutePath(),f).getAbsolutePath());
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	public void writePropertiesFile(String key, String data,String filePath) {
		System.out.println("Writing");
        FileOutputStream fileOut = null;
        FileInputStream fileIn = null;
        try {
            Properties configProperty = new Properties();

            File file = new File(filePath);
            fileIn = new FileInputStream(file);
            configProperty.load(fileIn);
            configProperty.setProperty(key, data);
            fileOut = new FileOutputStream(file);
            configProperty.store(fileOut, "sample properties");

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

            try {
                fileOut.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
	
	public void parseNarratorScript(String URL)
	{
	}
	
	public void initializeResources()
	{
		if(checkPresentation())
		{
			List<String> audioList=filterFiles("^.*[0-9].mp3");
			List<String> imageList = filterFiles("^.*[0-9].jpg");
			Collections.sort(audioList);
			Collections.sort(imageList);
			
			if(audioList.size()==imageList.size())
			{
				//Create the res files for the slides
				for(int i=0;i<audioList.size();i++)
				{
					File audioFile=new File(new File(this.getProjectLocalURL(),"presentation").getAbsolutePath(),(i+1)+".mp3");
					File imageFile= new File(new File(this.getProjectLocalURL(),"presentation").getAbsolutePath(),(i+1)+".jpg");
					File f= new File(new File(this.getProjectLocalURL(),"resources").getAbsolutePath(),(i+1)+".res");
					try {
						f.createNewFile();
					} catch (IOException e) {
						e.printStackTrace();
					}
					writePropertiesFile("type", "slide", f.getAbsolutePath());
					writePropertiesFile("audio",audioFile.getAbsolutePath(), f.getAbsolutePath());
					writePropertiesFile("image", imageFile.getAbsolutePath(), f.getAbsolutePath());
				}
			}
			//Create the res files for the video
			System.out.println("Done res slides");
		}
		else
		{
			System.out.println("Audio and Image Files are not completely associated.");
			return;
		}
		
		
	}
	
	private boolean checkPresentation() {
		generateImagesFromPresentation();
		convertWAVsToMp3();
		List<String> imageList= filterFiles("^.*[0-9].jpg");
		List<String> audioList= filterFiles("^.*[0-9].wav");
		List<String> buffer = new ArrayList<String>();
		for(int i=0;i<imageList.size();i++)
		{	
			String s= imageList.get(i);
			buffer.add(s.split(".jpg")[0]);
		}
		imageList = new ArrayList<String>(buffer);
		buffer = new ArrayList<String>();
		for(int i=0;i<audioList.size();i++)
		{	
			String s= audioList.get(i);
			buffer.add(s.split(".wav")[0]);
		}
		audioList = new ArrayList<String>(buffer);
		Set<String> imageSet = new HashSet<String>(imageList);
		Set<String> audioSet = new HashSet<String>(audioList);
		imageSet.retainAll(audioSet);
		if(imageSet.size()==audioSet.size())
			{
			
			return true;
			
			}
		else
			return false;
	}

	private void convertWAVsToMp3() {
		String path= (new File(this.getProjectLocalURL()).getParentFile().getParentFile().getAbsolutePath());
		//.getParentFile()).getParentFile()).getAbsolutePath()),"ffmpeg"); //TODO XXX PLEASE CHANGE IT BACK TO NORMAL
		FFMPEGWrapper ffmpegWrapper = new FFMPEGWrapper(path);/// 
		List<String> audioList= filterFiles(".*.wav");
		String parentDirectoryPresentation= new File(this.getProjectLocalURL(),"presentation").getAbsolutePath();
		for (String s: audioList)
		{
			try {
				if(!new File(parentDirectoryPresentation,s.split(".wav")[0]+".mp3").exists())
				ffmpegWrapper.convertWavToMp3(new File(parentDirectoryPresentation,s.split(".wav")[0]+".wav").getAbsolutePath(), new File(parentDirectoryPresentation,s.split(".wav")[0]+".mp3").getAbsolutePath());
			} catch (IOException | InterruptedException e) {	
				e.printStackTrace();
			}	
		}
	}

	private List<String> filterFiles(final String regex) {
		File parentDirectoryPresentation= new File(this.getProjectLocalURL(),"presentation");
		//System.out.println(parentDirectoryPresentation.getAbsolutePath());
		final List<String> originalList= new ArrayList<String>( Arrays.asList( parentDirectoryPresentation.list() ) );
		final Predicate<String> matchesWithRegex = new Predicate<String>() {
	        @Override 
	        public boolean apply(String str) {
	            return str.matches(regex);
	        }
		};
        Iterable<String> iterable = Iterables.filter(originalList, matchesWithRegex);
        List<String> resultList = Lists.newArrayList(iterable);
		return resultList;
	}

	private void generateImagesFromPresentation() {
		List<String> fileList=filterFiles(".*.pptx");
		if(fileList.isEmpty())
		{
			System.out.println("Error");
		}
		else
		{
			PPTToImages pptToImages = new PPTToImages(this.getProjectLocalURL());
			pptToImages.getImages();
		}
	}

	public static void main(String[] args)
	{
		
		//Project p = new Project("sasa","F:\\IITB\\Nitishavp-lokavidya-desktop-client-4787a50b17fb\\Projects");
		//p.initializeResources();
		//p.stitch();
		
		
		//p.convertWAVsToMp3();
		//PPTToImages t = new PPTToImages("/home/sanket/development/bitbucketrepos/Nitishavp-lokavidya-desktop-client-4787a50b17fb/Projects/demoproj");
		//t.getImages();
		//p.stitch();
		/*FFMPEGWrapper ffmpegWrapper = new FFMPEGWrapper("/home/sanket/Downloads");
		try {
			ffmpegWrapper.stitchImageAndAudio("/home/sanket/workspace/xugglerdemo/tempo/1.jpg","/home/sanket/workspace/xugglerdemo/tempo/sample.mp3","/home/sanket/workspace/xugglerdemo/tempo/final.mp4","/home/sanket/workspace/xugglerdemo/tempo/temp.mp4");
			List<String> videoPaths= new ArrayList<String>();
			videoPaths.add("/home/sanket/workspace/xugglerdemo/tempo/final.mp4");
			videoPaths.add("/home/sanket/workspace/xugglerdemo/tempo/final.mp4");
			videoPaths.add("/home/sanket/workspace/xugglerdemo/tempo/final.mp4");
			videoPaths.add("/home/sanket/workspace/xugglerdemo/tempo/final.mp4");
			videoPaths.add("/home/sanket/workspace/xugglerdemo/tempo/final.mp4");
			ffmpegWrapper.stitchVideo(videoPaths, "/home/sanket/workspace/xugglerdemo/tempo", "/home/sanket/workspace/xugglerdemo/tempo/finalout.mp4");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}*
	}*/
		
}
