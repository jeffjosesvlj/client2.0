package data;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import gui.Call;
import poi.ImagesToPpt;

public class ConverttoDesktop {

	public static void copyFile( File from, File to ) {
		 //  Files.delete(to.toPath());
	    	
	    	try {
				Files.copy( from.toPath(), to.toPath() );
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	public ConverttoDesktop(String androidPath) {
		// TODO Auto-generated constructor stub
		Call.workspace.clean();
		String projName=new File(androidPath).getName(),fname,ext = ".jpg";
		File theDir=new File(androidPath,"images");
		if(theDir.exists())
		{
			for(File file: theDir.listFiles())
			{
				
				fname=file.getName();
				int i=fname.lastIndexOf(".");
				ext=fname.substring(i);
				String find=projName+".";
				String newstr=Call.workspace.name+"_";
				fname=fname.replace(find,newstr);
				File fimage=new File(Call.workspace.imagesPath,fname);
				
				copyFile(file, fimage);
							}
		}
		theDir=new File(androidPath,"audio");
		if(theDir.exists())
		{
			for(File file: theDir.listFiles())
			{
				fname=file.getName();
				String find=projName+".";
				String newstr=Call.workspace.name+"_";
				fname=fname.replace(find,newstr);
				File faudio=new File(Call.workspace.audioPath,fname);
				
				copyFile(file, faudio);
				
			}
		}
		new ImagesToPpt(ext);
		Call.workspace.populateProject();
	}

}
