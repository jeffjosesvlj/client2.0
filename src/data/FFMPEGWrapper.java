package data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

class StreamGobbler extends Thread
{
    InputStream is;
    String type;
    
    StreamGobbler(InputStream is, String type)
    {
        this.is = is;
        this.type = type;
    }
    
    public void run()
    {
        try
        {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line=null;
            while ( (line = br.readLine()) != null)
                System.out.println(type + ">" + line);    
            } catch (IOException ioe)
              {
                ioe.printStackTrace();  
              }
    }
}
public class FFMPEGWrapper {

	String pathExecutable;
	
	public FFMPEGWrapper(String sourcePath)
	{
		String osname=System.getProperty("os.name");
		System.out.println(osname);
		if (osname.contains("Windows"))
		{
			pathExecutable = new File(new File(new File(sourcePath,"ffmpeg").getAbsolutePath(),"bin"),"ffmpeg.exe").getAbsolutePath();
		}
		else
		{
			System.out.println("Setting to Linux");
			pathExecutable = new File(new File(sourcePath,"ffmpeg").getAbsolutePath(),"ffmpeg").getAbsolutePath();
			System.out.println(pathExecutable);
		}
	}
	
	public boolean stitchImageAndAudio(String imgPath, String audioPath, String videoPath,String tempPath) throws IOException, InterruptedException
	{
		/*
		 * Check if final or temp exists delete if it does.
		 */
		File temp=  new File(tempPath);
		File video = new File(videoPath);
		if (temp.exists())
			temp.delete();
		if (video.exists())
			video.delete();
		String cmd =" "+pathExecutable;//+" -help";
		String cmdext= " -loop 1 -i "+imgPath+" -c:v libx264 -t 2 -pix_fmt yuv420p -vf scale=320:240 "+tempPath;
		cmd +=cmdext;
		System.out.println("The cmd for audio stitching is: "+cmd);
		Runtime run;
		Process pr;
		BufferedReader buf;
		String line;
		try {
			run = Runtime.getRuntime();
			pr = run.exec(cmd);
			System.out.println("The output for stitching the image part is: ");
			
/*//			buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
//			line = "";
//			while ((line=buf.readLine())!=null) {
//			System.out.println(line);
//			pr.waitFor();
//			System.out.println("Wait for has ended ");
//			
//			buf = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
//			line = "";
//			while ((line=buf.readLine())!=null) {
//				System.out.println(line);
//			}
//			pr.waitFor();
	*/		
			
			// any error message?
            StreamGobbler errorGobbler = new 
                StreamGobbler(pr.getErrorStream(), "ERROR");            
            
            // any output?
            StreamGobbler outputGobbler = new 
                StreamGobbler(pr.getInputStream(), "OUTPUT");
                
            // kick them off
            errorGobbler.start();
            outputGobbler.start();
                                    
            // any error???
            int exitVal = pr.waitFor();
            System.out.println("ExitValue: " + exitVal); 
			System.out.println("Wait for has ended ");
		
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		cmd =" "+pathExecutable;//+" -help";
		cmdext=" -i "+audioPath+" -i "+tempPath+" -c:a copy -vcodec copy -strict -2 "+videoPath;
		cmd +=cmdext;
		System.out.println("The cmd for audio stitching is: "+cmd);
		run = Runtime.getRuntime();
		System.out.println("The output for stitching the audio part is: ");
		pr = run.exec(cmd);
		pr.waitFor();
		System.out.println("Wait for has ended ");
		buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
		line = "";
		while ((line=buf.readLine())!=null) {
		System.out.println(line);
		}
		return true;
	}
	
	public boolean stitchVideo(List<String> videoPaths,String tempPath,String finalPath) throws IOException, InterruptedException
	{
		//Writing files to order.
        File orderVideoFile = new File(tempPath);
		try {
			FileOutputStream out = new FileOutputStream(orderVideoFile);
            for(int i=0;i<videoPaths.size();i++) {
                String data="file '"+videoPaths.get(i)+"'\n";
                out.write(data.getBytes());
            }
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
		String cmd=" "+pathExecutable;//+" -help";
		String cmdext= " -f concat -i    "+orderVideoFile.getAbsolutePath()+" -codec copy  "+finalPath;
		cmd+=cmdext;
		Runtime run = Runtime.getRuntime();
		Process pr = run.exec(cmd);
		pr.waitFor();
		BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
		String line = "";
		while ((line=buf.readLine())!=null) {
		System.out.println(line);
		}
		return false;
		
	}
	
	public  boolean convertWavToMp3(String wavPath, String mp3Path) throws IOException, InterruptedException
	{
		String cmd =" "+pathExecutable;//+" -help";
		String cmdext= " -i "+wavPath+" -f mp2 "+mp3Path;
		cmd +=cmdext;
		Runtime run = Runtime.getRuntime();
		Process pr = run.exec(cmd);
		pr.waitFor();
		BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
		String line = "";
		while ((line=buf.readLine())!=null) {
		System.out.println(line);
		}
		return true;
	}
}
