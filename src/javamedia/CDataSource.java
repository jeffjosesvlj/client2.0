/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javamedia;

import java.io.IOException;
import javax.media.MediaLocator;
import javax.media.Time;
import javax.media.protocol.PushBufferDataSource;
import javax.media.protocol.PushBufferStream;

/**
 *
 * @author Joey
 */
public class CDataSource extends PushBufferDataSource {

protected Object [] controls = new Object[0];
protected boolean started = false;
protected String contentType = "raw";
protected boolean connected = false;
protected Time duration = DURATION_UNBOUNDED;
protected LiveStream [] streams = null;
protected LiveStream stream = null;

public MediaLocator ml;
 
    public CDataSource(MediaLocator medl) {
        this.ml = medl;
    }

    public PushBufferStream [] getStreams() {
    if (streams == null) {
        streams = new LiveStream[1];
        stream = streams[0] = new LiveStream(ml);//! this line
    }
    return streams;
    }


public String getContentType() {
if (!connected){
System.err.println("Error: DataSource not connected");
return null;
}
return contentType;
}

public void connect() throws IOException {
if (connected)
return;
connected = true;
}

public void disconnect() {
try {
if (started)
stop();
} catch (IOException e) {}
connected = false;
}

public void start() throws IOException {
// we need to throw error if connect() has not been called
if (!connected)
throw new java.lang.Error("DataSource must be connected before it can be started");
if (started)
return;
started = true;
stream.start(true);
}

public void stop() throws IOException {
if ((!connected) || (!started))
return;
started = false;
stream.start(false);
}

public Object [] getControls() {
return controls;
}

public Object getControl(String controlType) {
return null;
}

public Time getDuration() {
return duration;
}

}

